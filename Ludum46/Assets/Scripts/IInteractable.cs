﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IInteractable
{
    int Priority { get; }

    Transform instructionPosition { get; }

    bool Interact(Interact interact);

    void StopInteract();
}
