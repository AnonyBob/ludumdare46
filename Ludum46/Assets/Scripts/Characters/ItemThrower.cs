﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Item
{
    None,
    Milk,
    Butter,
    Cheese,
    Hamburger
}

public class ItemThrower : MonoBehaviour
{
    public Item currentItem;
    public Transform throwHolder;
    public float throwIntensity = 30f;

    public ThrowableItem milkPrefab;
    public ThrowableItem butterPrefab;
    public ThrowableItem cheesePrefab;
    public ThrowableItem hamburgerPrefab;

    public AudioSource throwSound;

    private SelectedTool selectedTool;
    private Camera mainCamera;
    private ThrowableItem itemToThrow;
    private bool itemWasThrown;

    public Queue<Item> followUps = new Queue<Item>();

    public void SetItem(Item item)
    {
        currentItem = item;
        if(currentItem != Item.None)
        {
            itemToThrow = Instantiate<ThrowableItem>(GetItemPrefab(), Vector3.zero, Quaternion.identity, throwHolder);
            itemToThrow.transform.localPosition = Vector3.zero;
            itemToThrow.WaitToThrow();
            var pickup = itemToThrow.GetComponent<Pickup>();
            if(pickup != null)
            {
                Destroy(pickup);
            }
        }
    }

    public void AddFollowup(Item item)
    {
        followUps.Enqueue(item);
    }

    private void Start()
    {
        mainCamera = Camera.main;
        selectedTool = GetComponent<SelectedTool>();
    }

    private void Update()
    {
        if(Input.GetButtonDown("Fire1") && currentItem != Item.None &&
            selectedTool.selected == Tool.Item)
        {
            ThrowItem();
        }
    }

    private void LateUpdate()
    {
        if(itemWasThrown)
        {
            itemWasThrown = false;
            if(followUps.Count > 0)
            {
                SetItem(followUps.Dequeue());
            }
            else
            {
                SetItem(Item.None);
                selectedTool.ReturnToPrevious();
            }
        }
    }

    private void ThrowItem()
    {
        var position = mainCamera.ScreenToWorldPoint(Input.mousePosition);
        position.z = 0;

        var throwDirection = (position - transform.position).normalized;
        itemToThrow.Launch(throwDirection * throwIntensity);
        itemWasThrown = true;
        throwSound.Play();
    }

    private ThrowableItem GetItemPrefab()
    {
        switch(currentItem)
        {
            case Item.Milk:
                return milkPrefab;
            case Item.Cheese:
                return cheesePrefab;
            case Item.Butter:
                return butterPrefab;
            case Item.Hamburger:
                return hamburgerPrefab;
        }

        return null;
    }
}
