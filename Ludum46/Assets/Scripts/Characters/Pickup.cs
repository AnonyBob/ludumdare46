﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup : MonoBehaviour, IInteractable
{
    public int Priority => 12;

    public Transform instructionPosition => anchorPosition;

    public Item item;
    public ThrowableItem throwable;
    public Transform anchorPosition;

    private void Start()
    {
        throwable = GetComponent<ThrowableItem>();
    }

    public bool Interact(Interact interact)
    {
        if(!throwable.waitingForPickup)
        {
            return false;
        }

        var itemThrow = interact.GetComponentInParent<ItemThrower>();
        var tool = interact.GetComponentInParent<SelectedTool>();
        
        if(tool.selected == Tool.Item && itemThrow.currentItem != item)
        {
            return false;
        }
        else if(tool.selected == Tool.Item && itemThrow.currentItem == item)
        {
            itemThrow.AddFollowup(item);
        }
        else
        {
            tool.selected = Tool.Item;
            itemThrow.SetItem(item);
        }

        Destroy(gameObject);
        return false;
    }

    public void StopInteract()
    {
        
    }
}
