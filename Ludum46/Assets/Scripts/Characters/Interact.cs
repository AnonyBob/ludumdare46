﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interact : MonoBehaviour
{
    public HashSet<IInteractable> interactables = new HashSet<IInteractable>();
    public IInteractable interacting;
    public Transform interactionInstruction;

    public void StopInteraction()
    {
        if(interacting != null)
        {
            interacting.StopInteract();
            interacting = null;
        }
    }

    private void Update()
    {
        if(Input.GetButtonDown("Interact"))
        {
            if(interacting != null)
            {
                interacting.StopInteract();
                interacting = null;
            }
            else if (interactables.Count > 0)
            {
                SelectInteractable();
            }
        }
    }

    private void LateUpdate()
    {
        if (interacting == null && interactables.Count > 0)
        {
            IInteractable highest = null;
            foreach(var interactable in interactables)
            {
                if (highest == null || highest.Priority < interactable.Priority)
                {
                    highest = interactable;
                }
            }

            interactionInstruction.gameObject.SetActive(true);
            interactionInstruction.position = highest.instructionPosition.position;
        }
        else
        {
            interactionInstruction.gameObject.SetActive(false);
        }
    }

    private void SelectInteractable()
    {
        IInteractable highest = null;
        foreach(var interactable in interactables)
        {
            if(highest == null || highest.Priority < interactable.Priority)
            {
                highest = interactable;
            }
        }

        if(highest != null)
        {
            if (highest.Interact(this))
            {
                interacting = highest;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        var interactable = collision.GetComponent<IInteractable>();
        if(interactable != null)
        {
            interactables.Add(interactable);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        var interactable = collision.GetComponent<IInteractable>();
        if (interactable != null)
        {
            interactables.Remove(interactable);
        }
    }
}
