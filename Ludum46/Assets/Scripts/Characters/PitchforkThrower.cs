﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PitchforkThrower : MonoBehaviour
{
    public Camera mainCamera;
    public float timeBetweenThrows = 0.2f;
    public float throwIntensity = 30f;
    public GameObject coinInstruction;
    public Transform intructionPoint;

    public Pitchfork pitchforkPrefab;

    public float sinceLastThrow;

    private SelectedTool selectedTool;

    private void Start()
    {
        sinceLastThrow = timeBetweenThrows;
        selectedTool = GetComponent<SelectedTool>();
    }

    private void Update()
    {
        if(sinceLastThrow < timeBetweenThrows)
        {
            sinceLastThrow += Time.deltaTime;
        }

        if(Input.GetButtonDown("Fire1") && selectedTool.selected == Tool.Pitchfork)
        {
            ThrowPitchfork();
        }
    }

    private void ThrowPitchfork()
    {
        if(sinceLastThrow >= timeBetweenThrows)
        {
            sinceLastThrow = 0;
            if(Economy.Deduct(Tool.Pitchfork))
            {
                var position = mainCamera.ScreenToWorldPoint(Input.mousePosition);
                position.z = 0;

                var throwDirection = (position - transform.position).normalized;
                var throwRotation = Quaternion.Euler(0, 0, Mathf.Atan2(throwDirection.y, throwDirection.x) * Mathf.Rad2Deg - 90);
                var pitchFork = Instantiate<Pitchfork>(pitchforkPrefab, transform.position, throwRotation, transform.parent);
                pitchFork.Launch(throwDirection * throwIntensity);
            }
            else
            {
                Instantiate(coinInstruction, intructionPoint.position, Quaternion.identity, intructionPoint);
            }
        }
    }
}
