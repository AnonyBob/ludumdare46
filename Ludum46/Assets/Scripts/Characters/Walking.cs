﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Walking : MonoBehaviour
{
    public Mover mover;
    public AudioSource walkSource;
    public ParticleSystem dust;

    private void Update()
    {
        if(mover.onGround && Mathf.Abs(mover.body.velocity.x) > 0)
        {
            if(!dust.isPlaying)
            {
                dust.Play();
            }

            if(!walkSource.isPlaying)
            {
                walkSource.Play();
            }
        }
        else
        {
            if(walkSource.isPlaying)
            {
                walkSource.Stop();
            }
            if(dust.isPlaying)
            {
                dust.Stop();
            }
        }
    }
}
