﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeedThrower : MonoBehaviour
{
    public Camera mainCamera;
    public float timeBetweenThrows = 0.2f;
    public float throwIntensity = 30f;
    public GameObject coinInstruction;
    public Transform instructionPoint;
    public AudioSource throwSound;

    public Seed seedPrefab;

    public float sinceLastThrow;

    public event Action OnSeedThrow;

    private SelectedTool selectedTool;

    private void Start()
    {
        sinceLastThrow = timeBetweenThrows;
        selectedTool = GetComponent<SelectedTool>();
    }

    private void Update()
    {
        if (sinceLastThrow < timeBetweenThrows)
        {
            sinceLastThrow += Time.deltaTime;
        }

        if (Input.GetButtonDown("Fire1") && selectedTool.selected == Tool.Seed)
        {
            ThrowSeeds();
        }
    }

    private void ThrowSeeds()
    {
        if (sinceLastThrow >= timeBetweenThrows)
        {
            sinceLastThrow = 0;
            if(Economy.Deduct(Tool.Seed))
            {
                var position = mainCamera.ScreenToWorldPoint(Input.mousePosition);
                position.z = 0;

                var throwDirection = (position - transform.position).normalized;
                var seed = Instantiate<Seed>(seedPrefab, transform.position, Quaternion.identity, transform.parent);
                seed.Launch(throwDirection * throwIntensity);

                OnSeedThrow?.Invoke();
                throwSound.Play();
            }
            else
            {
                Instantiate(coinInstruction, instructionPoint.position, Quaternion.identity, instructionPoint);
            }
        }
    }
}
