﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Seed : MonoBehaviour
{
    public Rigidbody2D body;
    public float seedRange = 2;
    public LayerMask mask;

    public float rotationSpeed = 23;
    public Transform collisionSpawn;

    public void Launch(Vector2 direction)
    {
        body.angularVelocity = rotationSpeed;
        body.velocity = direction;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collisionSpawn != null)
        {
            Instantiate(collisionSpawn, transform.position, Quaternion.identity, transform.parent);
        }

        var cast = Physics2D.CircleCastAll(transform.position, seedRange, Vector2.zero, 0, mask);
        foreach(var castHit in cast)
        {
            var food = castHit.collider.GetComponent<FoodSource>();
            if(food != null)
            {
                food.Restore(food.startingAmount / 2);
            }
        }

        Destroy(gameObject);
    }
}
