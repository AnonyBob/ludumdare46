﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MilkGatherer : MonoBehaviour
{
    public Transform instructionPoint;
    public GameObject instruction;

    private SelectedTool selectedTool;
    private Interact interact;
    private ItemThrower itemThrower;

    private void Start()
    {
        interact = GetComponentInChildren<Interact>();
        itemThrower = GetComponent<ItemThrower>();
        selectedTool = GetComponent<SelectedTool>();
    }

    private void Update()
    {
        if(selectedTool.selected != Tool.Milk)
        {
            return;
        }

        if (Input.GetButtonDown("Fire1"))
        {
            AttemptToCollectMilk();
        }
    }

    private void AttemptToCollectMilk()
    {
        var didGetMilk = false;
        foreach(var interactable in interact.interactables)
        {
            if(interactable is CowPickup cowPickup)
            {
                cowPickup.cow.Milk();
                itemThrower.SetItem(Item.Milk);
                selectedTool.selected = Tool.Item;
                didGetMilk = true;
                break;
            }
        }

        if(!didGetMilk)
        {
            Instantiate(instruction, instructionPoint.position, Quaternion.identity, instructionPoint);
        }
    }
}
