﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CowHolder : MonoBehaviour
{
    public float throwIntensity = 30;
    public float throwTime = 5f;

    private SelectedTool selectedTool;
    private Camera mainCamera;

    private void Start()
    {
        mainCamera = Camera.main;
        selectedTool = GetComponentInParent<SelectedTool>();
    }

    private void Update()
    {
        if (Input.GetButtonDown("Fire1") && selectedTool.selected == Tool.Cow)
        {
            ThrowCow();
        }
    }

    private void ThrowCow()
    {
        var interact = GetComponent<Interact>();
        if(interact != null)
        {
            var interaction = interact.interacting as CowPickup;
            if(interaction != null)
            {
                interact.StopInteraction();

                var position = mainCamera.ScreenToWorldPoint(Input.mousePosition);
                position.z = 0;

                var throwDirection = (position - transform.position).normalized;
                interaction.cow.mover.Launch(throwDirection * throwIntensity, throwTime);
                interaction.cow.Drop();
                interaction.cow.throwSound.Play();
            }
            
        }
    }
}
