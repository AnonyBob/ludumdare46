﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Tool
{
    Pitchfork,
    Seed,
    Cow,
    Milk,
    Item
}

public class SelectedTool : MonoBehaviour
{
    public Tool selected;

    public Tool[] selectionOptions = new Tool[3];

    public int selectedOptionIndex;

    public void ReturnToPrevious()
    {
        selected = selectionOptions[selectedOptionIndex];
    }

    private void Update()
    {
        var otherOption = true;
        for(var i = 0; i < selectionOptions.Length; ++i)
        {
            if(selectionOptions[i] == selected)
            {
                otherOption = false;
                break;
            }
        }

        if(otherOption)
        {
            return;
        }

        var previousIndex = selectedOptionIndex;
        if(Input.GetAxis("Mouse ScrollWheel") > 0)
        {
            selectedOptionIndex++;
        }
        else if(Input.GetAxis("Mouse ScrollWheel") < 0)
        {
            selectedOptionIndex--;
        }

        if(selectedOptionIndex < 0)
        {
            selectedOptionIndex = selectionOptions.Length - 1;
        }
        else if(selectedOptionIndex >= selectionOptions.Length)
        {
            selectedOptionIndex = selectedOptionIndex % selectionOptions.Length;
        }

        if(previousIndex != selectedOptionIndex)
        {
            selected = selectionOptions[selectedOptionIndex];
        }
    }
}
