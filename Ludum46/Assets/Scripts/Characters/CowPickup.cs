﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CowPickup : MonoBehaviour, IInteractable
{
    public int Priority => 10;

    public Transform instructionPosition => instructionAnchor;

    public Cow cow;
    public Transform instructionAnchor;

    public Transform previousParent;
    public SelectedTool selectedTool;
    public Tool previousTool;

    private bool fromDestruction;

    private void Start()
    {
        cow = GetComponent<Cow>();
    }

    private void OnDestroy()
    {
        var interact = FindObjectOfType<Interact>();
        fromDestruction = true;
        if(interact != null && interact.interacting == this)
        {
            interact.StopInteraction();
        }
    }

    public bool Interact(Interact interact)
    {
        if(cow != null)
        {
            cow.Pickup();

            var holder = interact.GetComponentInChildren<CowHolder>();

            previousParent = cow.transform.parent;
            cow.transform.rotation = Quaternion.Euler(0, 0, 180);
            cow.transform.parent = holder.transform;
            cow.transform.localPosition = Vector3.zero;

            selectedTool = holder.GetComponentInParent<SelectedTool>();
            previousTool = selectedTool.selected;
            selectedTool.selected = Tool.Cow;
            return true;
        }

        return false;
    }

    public void StopInteract()
    {
        selectedTool.selected = previousTool;
        if (cow != null && !fromDestruction)
        {
            cow.Drop();
            cow.mover.body.rotation = 0;
            cow.transform.parent = previousParent;
            cow.transform.rotation = Quaternion.identity;
        }
    }
}
