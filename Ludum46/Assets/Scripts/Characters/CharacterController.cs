﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterController : MonoBehaviour
{
    public Mover mover;

    public float speed;
    public float detectionBuffer = 0.01f;
    public Animator animator;
    public AudioSource jumpSound;

    [Header("Jumping")]
    public float jumpSpeed = 10;
    public float secondJumpSpeed = 8;
    public float jumpSlowdownMultiplier = 0.5f;
    public float defaultGravity = 5;
    public float fallingGravity = 7;

    private void Update()
    {
        CalculateMovement();
    }

    private void CalculateMovement()
    {
        mover.targetMovement = Input.GetAxis("Horizontal") * speed;

        var velocity = mover.body.velocity;
        if(Input.GetButtonDown("Jump") && mover.onGround)
        {
            velocity.y = jumpSpeed;
            animator.SetTrigger("Jump");
            jumpSound.Play();
        }
        else if(Input.GetButtonUp("Jump"))
        {
            if(mover.body.velocity.y > 0)
            {
                velocity.y *= jumpSlowdownMultiplier;
            }
        }
        else if(Input.GetButtonDown("Jump") && mover.canSecondJump)
        {
            velocity.y = secondJumpSpeed;
            mover.canSecondJump = false;
            animator.SetTrigger("Jump");
            jumpSound.Play();
        }

        mover.body.velocity = velocity;
        animator.SetBool("Falling", mover.body.velocity.y < -0.1f);
        animator.SetFloat("Speed", Mathf.Abs(mover.body.velocity.x));
    }
}
