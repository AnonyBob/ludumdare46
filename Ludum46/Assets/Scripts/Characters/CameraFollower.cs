﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollower : MonoBehaviour
{
    public Rigidbody2D target;
    public float smoothness;
    public Vector3 offset;
    public float velocityDirectionOffset;

    public Vector3 velocity;

    private void LateUpdate()
    {
        var velocityDirection = target.velocity.normalized * velocityDirectionOffset;
        transform.position = Vector3.SmoothDamp(transform.position, (Vector3)(target.position + velocityDirection) + offset, ref velocity, smoothness);
    }
}
