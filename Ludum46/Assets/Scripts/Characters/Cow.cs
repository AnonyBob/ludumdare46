﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cow : MonoBehaviour
{
    public CowName cowName;

    public Mover mover;
    public float timeScared;
    public AudioSource milkSound;
    public AudioSource pickupSound;
    public AudioSource throwSound;

    [Header("Eating")]
    public float timeBetweenEats;
    public float timeBetweenHungry;
    public float minDistanceToFoodSource;
    public Transform head;
    public Collider2D collider;
    public Health health;
    public int hungerLoss;
    public int milkLoss;
    public Animator animator;
    public GameObject grassInstruction;
    public Transform instructionPoint;

    [Header("Movement")]
    public float scaredSpeed;
    public float normalSpeed;

    [Header("Debug")]
    public float timeSinceLastEat;
    public FoodSource nextFoodSource;
    public bool isHungry;
    public float timeSinceLastHungry;
    public int hungryDirection;

    public bool isPickedUp;

    public bool isScared;
    public Transform scaredTarget;
    public float timeSinceLastScared;

    private void Update()
    {
        CalculateMovement();
    }

    private void LateUpdate()
    {
        animator.SetFloat("Speed", Mathf.Abs(mover.body.velocity.x));
        animator.SetBool("Falling", mover.body.velocity.y < 0);
    }

    private void CalculateMovement()
    {
        if(transform.localScale.x < 0)
        {
            transform.localScale = Vector3.one;
        }

        mover.targetMovement = 0;

        if(isPickedUp)
        {
            return;
        }

        if(isScared)
        {
            mover.targetMovement = GetDirectionOfFear() * scaredSpeed; 
            timeSinceLastScared += Time.deltaTime;
            if(timeSinceLastScared >= timeScared)
            {
                isScared = false;
            }
        }
        else if(isHungry)
        {
            mover.targetMovement = normalSpeed * 0.3f * hungryDirection;
            timeSinceLastHungry += Time.deltaTime;
            if(timeSinceLastHungry >= timeBetweenHungry)
            {
                isHungry = false;
                Starve();
            }

            timeSinceLastEat += Time.deltaTime;
        }
        else
        {
            //If the cow is hungry and needs to eat.
            if(timeBetweenEats < timeSinceLastEat)
            {
                if(CanEat())
                {
                    Eat();
                }

                timeSinceLastEat = 0;
            }

            MoveToFood();
            timeSinceLastEat += Time.deltaTime;
        }
    }

    private void OnDestroy()
    {
        if(nextFoodSource != null && nextFoodSource.Claimer == this)
        {
            nextFoodSource.Free();
        }
    }

    public void Scare(Transform target)
    {
        if(isPickedUp)
        {
            return;
        }

        isScared = true;
        scaredTarget = target;
        if(nextFoodSource != null && nextFoodSource.Claimer == this)
        {
            nextFoodSource.Free();
        }
    }

    public void Pickup()
    {
        mover.body.isKinematic = true;
        mover.body.constraints = RigidbodyConstraints2D.None;
        isPickedUp = true;
        pickupSound.Play();
    }

    public void Drop()
    {
        nextFoodSource = null;

        mover.body.isKinematic = false;
        mover.body.constraints = RigidbodyConstraints2D.FreezeRotation;
        isPickedUp = false;
        transform.localScale = Vector3.one;
    }

    public void Milk()
    {
        timeSinceLastEat = timeBetweenEats; //Make it immediately hungry
        health.Deduct(milkLoss);
        milkSound.Play();
    }

    private void Starve()
    {
        Instantiate(grassInstruction, instructionPoint.position, Quaternion.identity, instructionPoint);
    }

    private bool CanEat()
    {
        return nextFoodSource != null && 
            nextFoodSource.Amount > 0 &&
            (nextFoodSource.Claimer == null || nextFoodSource.Claimer == this) &&
            mover.onGround &&
            Mathf.Abs(nextFoodSource.position.x - head.position.x) <= minDistanceToFoodSource &&
            nextFoodSource.position.y <= head.position.y;
    }

    private void Eat()
    {
        if(nextFoodSource != null)
        {
            animator.SetTrigger("Eat");
            nextFoodSource.Eat();
            health.Credit(hungerLoss);
            if(nextFoodSource.Amount <= 0)
            {
                nextFoodSource.Free();
                nextFoodSource = null;
            }
        }
    }

    private void LocateNextFoodSource()
    {
        if(nextFoodSource != null)
        {
            nextFoodSource.Free();
        }

        var foodSource = FoodSourceRegistry.GetClosestFoodSource(transform.position, this);
        nextFoodSource = foodSource;
    }

    private void MoveToFood()
    {
        if(nextFoodSource == null 
            || nextFoodSource.Amount == 0 
            || (nextFoodSource.Claimer != null && nextFoodSource.Claimer != this)
            || nextFoodSource.position.y > head.position.y)
        {
            LocateNextFoodSource();
        }

        if(CanEat())
        {
            return;
        }
        else if(nextFoodSource == null) //There is no food source. Panic!
        {
            isHungry = true;
            hungryDirection = Random.Range(-1, 2);
            timeSinceLastHungry = 0;
        }
        else if(mover.onGround)
        {
            mover.targetMovement = GetDirectionToFood() * normalSpeed;
            nextFoodSource.Claim(this);
        }
    }

    private float GetDirectionToFood()
    {
        //If the food is below me.
        if(nextFoodSource.position.y < head.position.y - 2)
        {
            var edge = EdgeRegistry.GetClosestEdgeForPlatform(head.position);
            if(edge != null)
            {
                return Mathf.Sign(edge.position.x - head.position.x);
            }
        }

        if(IsFoodWithinBody())
        {
            var direction = Mathf.Sign(head.position.x - nextFoodSource.position.x);
            var hit = Physics2D.BoxCast(transform.position, mover.castingSize, 0, new Vector2(direction, 0), collider.bounds.size.x, mover.mask);
            if(!hit)
            {
                return direction;
            }
            else
            {
                return -direction;
            }
        }


        return Mathf.Sign(nextFoodSource.position.x - head.position.x);
    }

    private float GetDirectionOfFear()
    {
        return 0;
    }

    private bool IsFoodWithinBody()
    {
        var bounds = collider.bounds;
        var pos = nextFoodSource.position.x;
        return bounds.min.x < pos && bounds.max.x > pos;
    }
}
