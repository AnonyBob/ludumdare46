﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pitchfork : MonoBehaviour
{
    public Rigidbody2D body;
    public float timeToDie = 2f;
    public AudioSource hitSound;

    public float timeCollided;
    public bool collided;

    public void Launch(Vector3 direction)
    {
        body.velocity = direction;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collided)
        {
            return;
        }

        var robot = collision.collider.GetComponent<Robot>();
        if(robot != null)
        {
            robot.Hit(collision);
        }
        else
        {
            hitSound.Play();
        }

        body.isKinematic = true;
        body.velocity = Vector2.zero;
        body.angularVelocity = 0;


        collided = true;
        transform.parent = collision.collider.transform;
        GetComponent<Collider2D>().enabled = false;
    }

    private void Update()
    {
        if(collided)
        {
            timeCollided += Time.deltaTime;
            if(timeCollided >= timeToDie)
            {
                Destroy(gameObject);
            }
        }
        else
        {
            var velocity = body.velocity.normalized;
            body.rotation = (Mathf.Atan2(velocity.y, velocity.x) * Mathf.Rad2Deg) - 90f;
        }

    }

    private void LateUpdate()
    {
        
    }
}
