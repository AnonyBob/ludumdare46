﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoneyPopper : MonoBehaviour
{
    public MoneyPop moneyPopPrefab;

    private void Start()
    {
        Economy.OnChanged += HandleChange;
    }

    private void OnDestroy()
    {
        Economy.OnChanged -= HandleChange;
    }

    private void HandleChange(int amount, Tool tool, Item item)
    {
        var pop = Instantiate<MoneyPop>(moneyPopPrefab, transform.position, Quaternion.identity);
        pop.Setup(amount);
    }
}
