﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowableItem : MonoBehaviour
{
    public Item itemType;
    public float rotationSpeed = 23;
    public Transform collisionSpawn;
    public bool waitingForPickup;
    public bool waitingForThrow;
    public string layerOnThrow = "Weapon";

    private Rigidbody2D body;

    private void Awake()
    {
        body = GetComponent<Rigidbody2D>();
    }

    public void WaitToThrow()
    {
        body.isKinematic = true;
        body.velocity = Vector2.zero;
        body.angularVelocity = 0;
        waitingForThrow = true;
    }

    public void Launch(Vector2 throwDirection)
    {
        body.transform.parent = null;
        body.isKinematic = false;
        body.velocity = throwDirection;
        body.angularVelocity = rotationSpeed;
        gameObject.layer = LayerMask.NameToLayer(layerOnThrow);
        waitingForThrow = false;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(waitingForPickup || waitingForThrow)
        {
            return;
        }

        if(!collision.collider.GetComponent<Truck>())
        {
            if (collisionSpawn != null)
            {
                Instantiate(collisionSpawn, transform.position, Quaternion.identity);
            }
        }

        Destroy(gameObject);
    }
}

