﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public static class EdibleTiles
{
    private static Tilemap _tilemap;

    public static void Refresh(Vector3 position, TileBase tile)
    {
        if(_tilemap == null)
        {
            _tilemap = Object.FindObjectOfType<Tilemap>();
        }

        var intPosition = new Vector3Int(Mathf.RoundToInt(position.x), Mathf.RoundToInt(position.y - 1), 0);
        _tilemap.SetTile(intPosition, tile);
    }
}
