﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class StatDisplay : MonoBehaviour
{
    public Image petunia;
    public Image margerie;
    public Image daisy;
    public Image indiana;

    public TextMeshProUGUI pitchforks;
    public TextMeshProUGUI milk;
    public TextMeshProUGUI butter;
    public TextMeshProUGUI cheese;
    public TextMeshProUGUI seeds;
    public TextMeshProUGUI hamburgers;

    private void Start()
    {
        var stats = Stats.instance;

        if(petunia != null)
        {
            petunia.color = stats.petuniaDead ? Color.black : petunia.color;
            margerie.color = stats.margerieDead ? Color.black : margerie.color;
            daisy.color = stats.daisyDead ? Color.black : daisy.color;
            indiana.color = stats.indianaDead ? Color.black : indiana.color;
        }

        pitchforks.text = $"x{stats.pitchforkThrown}";
        milk.text = $"x{stats.milkSold}";
        butter.text = $"x{stats.butterSold}";
        cheese.text = $"x{stats.cheeseSold}";
        seeds.text = $"x{stats.seedsThrown}";
        hamburgers.text = $"x{stats.burgersSold}";
    }
}
