﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover : MonoBehaviour
{
    public float detectionBuffer;
    public Rigidbody2D body;
    public Transform flipper;

    public float defaultGravity;
    public float fallingGravity;

    public Vector2 castingSize;
    public LayerMask mask;

    [Header("Debug")]
    public float targetMovement;
    public float? targetVerticalMovement;

    public bool onGround;
    public bool canSecondJump;
    public List<RaycastHit2D> results = new List<RaycastHit2D>();

    public Coroutine launchRoutine;

    public void Launch(Vector2 direction, float duration)
    {
        transform.localScale = Vector3.one;
        if(launchRoutine != null)
        {
            StopCoroutine(launchRoutine);
        }

        launchRoutine = StartCoroutine(DoLaunch(direction, duration));
    }

    private void FixedUpdate()
    {
        if(launchRoutine == null)
        {
            var velocity = body.velocity;
            velocity.x = targetMovement;
            if(targetVerticalMovement.HasValue)
            {
                velocity.y = targetVerticalMovement.Value;
            }

            onGround = false;

            var horizontalHit = Physics2D.BoxCast(transform.position, castingSize, 0, new Vector2(Mathf.Sign(targetMovement), 0), detectionBuffer, mask);
            if (horizontalHit.collider != null)
            {
                var dot = Vector2.Dot(velocity, horizontalHit.normal);
                if (dot < 0)
                {
                    velocity = velocity - (dot * horizontalHit.normal);
                }
            }

            var verticalHit = Physics2D.BoxCast(transform.position, castingSize, 0, Vector2.down, detectionBuffer, mask);
            if (verticalHit.collider != null)
            {
                onGround = true;
                canSecondJump = true;
            }

            if (onGround || velocity.y > 0)
            {
                body.gravityScale = defaultGravity;
            }
            else
            {
                body.gravityScale = fallingGravity;
            }

            body.velocity = velocity;

            if (flipper.localScale.x > 0 && body.velocity.x > 0)
            {
                flipper.localScale = new Vector3(-1, 1, 1);
            }
            else if (flipper.localScale.x < 0 && body.velocity.x < 0)
            {
                flipper.localScale = new Vector3(1, 1, 1);
            }
        }
        
    }

    private IEnumerator DoLaunch(Vector2 direction, float duration)
    {
        body.velocity = direction;
        var time = 0f;
        while(time < duration)
        {
            time += Time.deltaTime;
            yield return null;
        }

        launchRoutine = null;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(launchRoutine != null)
        {
            StopCoroutine(launchRoutine);
            launchRoutine = null;
        }
    }
}
