﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartGame : MonoBehaviour
{
    public MonoBehaviour[] enablers;

    private void Start()
    {
        foreach(var enabler in enablers)
        {
            enabler.enabled = false;
        }
    }

    [ContextMenu("Begin")]
    public void BeginGame()
    {
        foreach(var enabler in enablers)
        {
            enabler.enabled = true;
        }
    }
}
