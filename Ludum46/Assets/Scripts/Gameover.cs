﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Gameover : MonoBehaviour
{
    public string successScene;
    public string failScene;

    public LoadScene loader;

    public bool gameOver = false;
    public static Gameover instance;

    private void Start()
    {
        instance = this;
    }

    private void OnDestroy()
    {
        instance = null;
    }

    public static void EndGame(bool success)
    {
        if(instance == null)
        {
            return;
        }

        if(instance.gameOver)
        {
            return;
        }

        instance.gameOver = true;

        instance.loader.scene = success ? instance.successScene : instance.failScene;
        instance.loader.mode = LoadSceneMode.Additive;
        instance.loader.DoIt();
    }
}
