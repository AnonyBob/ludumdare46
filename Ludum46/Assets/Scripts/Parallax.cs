﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour
{
    public Camera mainCamera;
    public Vector2 effectValue;
    private Vector2 startPos;

    private void Start()
    {
        startPos = transform.position;
    }

    private void LateUpdate()
    {
        var distX = mainCamera.transform.position.x * effectValue.x;
        var distY = mainCamera.transform.position.y * effectValue.y;
        transform.position = new Vector3(startPos.x + distX, startPos.y + distY, transform.position.z);
    }
}
