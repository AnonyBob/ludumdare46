﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Falling : MonoBehaviour
{
    public AudioSource fallSound;
    public float maxVolume = 0.5f;
    public float minVolume = 0.2f;

    public float maxSpeed = 40;
    public float minSpeed = 10;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(Vector2.Dot(collision.relativeVelocity, Vector2.up) > 0)
        {
            var percentage = Mathf.Clamp01(collision.relativeVelocity.magnitude / (maxSpeed - minSpeed));
            fallSound.volume = Mathf.Lerp(minVolume, maxVolume, percentage);
            fallSound.Play();
        }
    }
}
