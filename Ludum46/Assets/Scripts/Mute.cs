﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mute : MonoBehaviour
{
    public static bool muted;

    public GameObject muteIcon;

    private void Start()
    {
        if(muteIcon != null)
        {
            muteIcon.SetActive(muted);
        }
    }

    public void Toggle()
    {
        AudioListener.volume = muted ? 1 : 0;
        muted = !muted;

        if(muteIcon != null)
        {
            muteIcon.SetActive(muted);
        }
    }
}
