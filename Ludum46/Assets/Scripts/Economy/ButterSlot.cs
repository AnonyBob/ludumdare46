﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButterSlot : MakerSlot
{
    public SpriteRenderer milk;

    public Color startColor;
    public Color endColor;

    public float rotateSpeed;

    private void Awake()
    {
        milk.gameObject.SetActive(false);
    }

    protected override IEnumerator MakeRoutine()
    {
        milk.gameObject.SetActive(true);

        while(true)
        {
            milk.color = Color.Lerp(endColor, startColor, timing.timeRemaining / timing.maxTiming);
            milk.transform.Rotate(0, 0, rotateSpeed * Time.deltaTime);
            yield return null;
        }
    }

    protected override void StopMaking()
    {
        milk.gameObject.SetActive(false);
    }
}
