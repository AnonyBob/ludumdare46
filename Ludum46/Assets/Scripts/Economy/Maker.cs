﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Maker : MonoBehaviour
{
    public ThrowableItem makePrefab;
    public float makeTime;
    public Item targetItem;
    public bool isMaking;

    public List<MakerSlot> slots = new List<MakerSlot>();

    public int Priority => 5;

    public class MakeTiming
    {
        public float timeRemaining;
        public float maxTiming;
        public bool isFilled;
    }

    private void Start()
    {
        foreach(var slotObject in GetComponentsInChildren<MakerSlot>())
        {
            slotObject.Setup(new MakeTiming() { maxTiming = makeTime });
            slots.Add(slotObject);
        }
    }

    private void Update()
    {
        isMaking = false;
        for(var i = 0; i < slots.Count; ++i)
        {
            if(slots[i].timing.isFilled)
            {
                slots[i].timing.timeRemaining -= Time.deltaTime;
                if(slots[i].timing.timeRemaining <= 0)
                {
                    slots[i].timing.isFilled = false;
                    SpawnItem(slots[i]);
                }
                else
                {
                    isMaking = true;
                }
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        var throwableItem = collision.collider.GetComponent<ThrowableItem>();
        if (throwableItem != null && throwableItem.itemType == targetItem)
        {
            BeginMaking();
        }
    }

    private void BeginMaking()
    {
        var slot = slots.FirstOrDefault<MakerSlot>(s => !s.timing.isFilled);
        if(slot != null)
        {
            slot.timing.isFilled = true;
            slot.timing.timeRemaining = makeTime;
        }
    }

    private void SpawnItem(MakerSlot slot)
    {
        var item = Instantiate<ThrowableItem>(makePrefab, slot.transform.position, Quaternion.identity, transform.parent);
        item.waitingForPickup = true;
    }
}
