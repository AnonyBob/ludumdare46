﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheeseSlot : MakerSlot
{
    public Transform pump;
    public float maxPumpHeight;
    public float minPumpHeight;
    public float pumpCycleTime;

    public Transform amount;
    public float pumpTime;

    private void Awake()
    {
        StopMaking();
    }

    protected override IEnumerator MakeRoutine()
    {
        while(true)
        {
            amount.localScale = new Vector3(1, 1 - (timing.timeRemaining / timing.maxTiming), 1);
            var position = pump.localPosition;
            position.y = Mathf.Lerp(minPumpHeight, maxPumpHeight, Mathf.PingPong(pumpTime, pumpCycleTime));
            pump.localPosition = position;
            yield return null;

            pumpTime += Time.deltaTime;
        }
    }

    protected override void StopMaking()
    {
        amount.localScale = new Vector3(1, 0, 1);
    }
}
