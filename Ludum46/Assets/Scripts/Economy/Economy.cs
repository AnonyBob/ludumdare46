﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Economy : MonoBehaviour
{
    public int milkReward = 50;
    public int cheeseReward = 200;
    public int butterReward = 75;
    public int hamburgerReward = 2000;

    public int seedCost = 20;
    public int pitchforkCost = 5;

    public int startingMoney = 20000;
    public int tickCost = 10;
    public float tickLength = 1; 

    public int currentMoney;
    public float timeSinceLastTick;

    public static Economy instance;

    public static event Action<int, Tool, Item> OnChanged;
    public static event Action<CowName> OnCowDied;

    private void Awake()
    {
        currentMoney = startingMoney;
        instance = this;
    }

    private void OnDestroy()
    {
        instance = null;
    }

    private void Update()
    {
        if(tickLength <= timeSinceLastTick)
        {
            currentMoney -= tickCost;
            timeSinceLastTick = 0;
        }

        timeSinceLastTick += Time.deltaTime;
    }

    public static void KillCow(CowName cow)
    {
        OnCowDied?.Invoke(cow);
    }

    public static int GetMoney()
    {
        return instance.currentMoney;
    }

    public static void Award(Item item)
    {
        var reward = 0;
        switch(item)
        {
            case Item.Milk:
                reward = instance.milkReward;
                break;
            case Item.Cheese:
                reward = instance.cheeseReward;
                break;
            case Item.Butter:
                reward = instance.butterReward;
                break;
            case Item.Hamburger:
                reward = instance.hamburgerReward;
                break;
        }

        OnChanged?.Invoke(reward, Tool.Item, item);
        instance.currentMoney += reward;
    }

    public static bool Deduct(Tool tool)
    {
        var deduction = 0;
        switch(tool)
        {
            case Tool.Pitchfork:
                deduction = instance.pitchforkCost;
                break;
            case Tool.Seed:
                deduction = instance.seedCost;
                break;
        }

        if(instance.currentMoney >= deduction)
        {
            OnChanged?.Invoke(-deduction, tool, Item.None);

            instance.currentMoney += -deduction;
            return true;
        }

        return false;
    }
}
