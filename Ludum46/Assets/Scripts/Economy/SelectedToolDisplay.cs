﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SelectedToolDisplay : MonoBehaviour
{
    public Image image;
    public TextMeshProUGUI text;

    public Sprite cheese;
    public Sprite milk;
    public Sprite seeds;
    public Sprite butter;
    public Sprite pitchfork;
    public Sprite hamburger;
    public Sprite cow;

    private SelectedTool tool;
    private ItemThrower thrower;
    private Sprite previous;
    private int previousAmount;

    private void Start()
    {
        tool = FindObjectOfType<SelectedTool>();
        thrower = FindObjectOfType<ItemThrower>();
        previous = image.sprite;
    }

    private void Update()
    {
        var sprite = GetSprite();
        if(sprite != previous)
        {
            previous = sprite;
            image.sprite = sprite;
        }

        text.gameObject.SetActive(tool.selected == Tool.Item);
        if (previousAmount != thrower.followUps.Count)
        {
            previousAmount = thrower.followUps.Count;
            text.text = $"x{thrower.followUps.Count + 1}";
        }
    }

    private Sprite GetSprite()
    {
        switch(tool.selected)
        {
            case Tool.Cow:
                return cow;
            case Tool.Milk:
                return milk;
            case Tool.Pitchfork:
                return pitchfork;
            case Tool.Seed:
                return seeds;
            case Tool.Item:
                return GetItemSprite();
        }

        return pitchfork;
    }

    private Sprite GetItemSprite()
    {
        switch(thrower.currentItem)
        {
            case Item.Butter:
                return butter;
            case Item.Cheese:
                return cheese;
            case Item.Milk:
                return milk;
            case Item.Hamburger:
                return hamburger;
        }

        return milk;
    }
}
