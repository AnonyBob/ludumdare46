﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Truck : MonoBehaviour
{
    public Action OnReceivedGoods;
    public AudioClip cashSound;
    public AudioSource rewardNoise;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        var throwableItem = collision.collider.GetComponent<ThrowableItem>();
        if(throwableItem != null)
        {
            Economy.Award(throwableItem.itemType);
            OnReceivedGoods?.Invoke();
            rewardNoise.PlayOneShot(cashSound);
        }
    }
}
