﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class MoneyDisplay : MonoBehaviour
{
    public TextMeshProUGUI text;
    private int previousMoney;

    private void Update()
    {
        var money = Economy.GetMoney();
        if(money != previousMoney)
        {
            previousMoney = money;
            text.text = money.ToString("N0");
        }
    }
}
