﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class MakerSlot : MonoBehaviour
{
    public Maker.MakeTiming timing;
    protected bool wasMaking = false;
    private Coroutine makingRoutine;

    public void Setup(Maker.MakeTiming timing)
    {
        this.timing = timing;
    }

    public void EndMaking()
    {
        if(makingRoutine != null)
        {
            StopCoroutine(makingRoutine);
            StopMaking();
        }
    }

    private void LateUpdate()
    {
        if(this.timing != null && this.timing.isFilled)
        {
            if(!wasMaking)
            {
                wasMaking = true;
                makingRoutine = StartCoroutine(MakeRoutine());
            }
        }
        else if(wasMaking)
        {
            wasMaking = false;
            EndMaking();
        }
    }

    protected abstract IEnumerator MakeRoutine();

    protected abstract void StopMaking();
}
