﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodSourceVolume : MonoBehaviour
{
    public Collider2D shapeBounds;
    public FoodSource prefab;

    [ContextMenu("Spawn Things")]
    private void SetupFoodSources()
    {
        var bounds = shapeBounds.bounds;
        var center = bounds.center;
        var extents = bounds.extents;
        extents.y = 0;
        extents.z = 0;

        var currentPoint = center - extents;
        currentPoint.x += 0.5f;
        var endPoint = center + extents;
        while(currentPoint.x < endPoint.x)
        {
#if UNITY_EDITOR

            var food = (FoodSource)UnityEditor.PrefabUtility.InstantiatePrefab(prefab, transform);
            food.transform.SetPositionAndRotation(currentPoint, Quaternion.identity);
#endif
            currentPoint.x += 1;
        }
    }
}
