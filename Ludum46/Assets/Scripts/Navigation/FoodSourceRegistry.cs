﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class FoodSourceRegistry
{
    private static HashSet<FoodSource> _food = new HashSet<FoodSource>();

    public static void AddFood(FoodSource source)
    {
        _food.Add(source);
    }

    public static void RemoveFood(FoodSource source)
    {
        _food.Remove(source);
    }

    public static FoodSource GetClosestFoodSource(Vector2 position, Cow cow)
    {
        FoodSource closest = null;
        var closestDistance = float.MaxValue;

        foreach(var source in _food)
        {
            //Skip sources that are zero or above us.
            if(source.Amount <= 0 || source.position.y > position.y || source.Claimer != null)
            {
                continue;
            }

            var distance = Vector2.Distance(source.position, position);
            if (Mathf.Abs(source.position.y - position.y) > 2)
            {
                continue;
            }

            if (distance < closestDistance)
            {
                
                closest = source;
                closestDistance = distance;
            }
        }

        return closest;
    }
}
