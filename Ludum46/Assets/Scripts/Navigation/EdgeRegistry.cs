﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class EdgeRegistry 
{
    private static HashSet<Edge> _edges = new HashSet<Edge>();

    public static void AddEdge(Edge edge)
    {
        _edges.Add(edge);
    }

    public static void RemoveEdge(Edge edge)
    {
        _edges.Remove(edge);
    }

    public static Edge GetClosestEdgeForPlatform(Vector2 position)
    {
        Edge closest = null;
        var closestDistance = float.MaxValue;

        foreach (var edge in _edges)
        {
            var distance = Vector2.Distance(edge.position, position);
            if(Mathf.Abs(edge.position.y - position.y) > 2)
            {
                distance += 100;
            }

            if (distance < closestDistance)
            {
                closest = edge;
                closestDistance = distance;
            }
        }

        return closest;
    }
}
