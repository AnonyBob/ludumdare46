﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class FoodSource : MonoBehaviour
{
    public int startingAmount;
    public TileBase fullTile;
    public TileBase halfTile;
    public TileBase emptyTile;

    [Header("Debug")]
    public int Amount;
    public Cow Claimer;

    public Vector2 position => transform.position;

    private void Start()
    {
        Amount = startingAmount;
        FoodSourceRegistry.AddFood(this);
    }

    private void OnDestroy()
    {
        FoodSourceRegistry.RemoveFood(this);
    }

    public void Eat()
    {
        Amount--;
        RefreshVisuals();
    }

    public void Restore(int amount)
    {
        Amount += Mathf.FloorToInt(amount);
        if(Amount >= startingAmount)
        {
            Amount = startingAmount;
        }

        RefreshVisuals();
    }

    public void Claim(Cow cow)
    {
        Claimer = cow;
    }

    public void Free()
    {
        Claimer = null;
    }

    public void RefreshVisuals()
    {
        var percentage = Mathf.Clamp01(((float)Amount) / startingAmount);
        var tile = fullTile;
        if (percentage <= 0)
        {
            tile = emptyTile;
        }
        else if (percentage <= 0.5)
        {
            tile = halfTile;
        }

        EdibleTiles.Refresh(transform.position, tile);
    }
}
