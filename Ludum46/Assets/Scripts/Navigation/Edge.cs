﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Edge : MonoBehaviour
{
    public Vector2 position => transform.position;

    private void Start()
    {
        EdgeRegistry.AddEdge(this);
    }

    private void OnDestroy()
    {
        EdgeRegistry.RemoveEdge(this);
    }
}
