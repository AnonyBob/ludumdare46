﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mosquito : Robot
{
    [Header("Mosquito Settings")]
    public float attackSpeed;

    public override void Hit(Collision2D collision)
    {
        body.velocity = -collision.relativeVelocity;
        body.constraints = RigidbodyConstraints2D.None;
        body.drag = body.drag * 0.5f;
        ai.enabled = false;
        Die();
    }

    protected override void DoAttack()
    {
        var direction = (target.position - transform.position).normalized;
        body.velocity = direction * attackSpeed;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(isDead)
        {
            return;
        }

        var robot = collision.collider.GetComponent<Robot>();
        if(attacking && robot == null)
        {
            attacking = false;
            var health = collision.collider.GetComponent<Health>();
            if(health != null)
            {
                health.Deduct(damageOnAttack);
            }
        }

        var normal = collision.GetContact(0).normal * (robot != null ? 0.5f : 1f);
        BounceBack(normal);
        AcquireTarget();
    }
}
