﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class RobotSpawner : MonoBehaviour
{
    [System.Serializable]
    public class RobotSpawnGroup
    {
        public List<Robot> robots = new List<Robot>();
        public float timeUntilSpawn;
    }

    [System.Serializable]
    public class RobotWave
    {
        public float timeUntilNextWave;
        public List<RobotSpawnGroup> spawnGroups = new List<RobotSpawnGroup>();
    }

    public RobotWave[] waves;
    public RobotSpawnPoint[] spawnPoints;

    [Header("Debug")]
    public int currentWave;
    public int currentGroup;

    public float timeSinceLastSpawn;
    public float timeSinceWaveEnded;

    public bool waveEnded = true;
    public bool gameComplete = false;

    public event Action OnWaveEnded;
    public event Action OnWaveStarted;

    public float lastCheckTime = 5f;
    public float timeSinceLastCheck = 0f;

    private void Update()
    {
        if(gameComplete)
        {
            return;
        }

        if(waves.Length <= currentWave && !gameComplete)
        {
            if(lastCheckTime <= timeSinceLastCheck)
            {
                timeSinceLastCheck = 0;
                if (!CheckIfBotsRemaining())
                {
                    gameComplete = true;
                    Gameover.EndGame(true);
                }
            }

            timeSinceLastCheck += Time.deltaTime;
            return;
        }

        if(waveEnded)
        {
            timeSinceWaveEnded += Time.deltaTime;
            if (timeSinceWaveEnded >= waves[currentWave].timeUntilNextWave)
            {
                StartWave();
            }
        }
        else if(waves[currentWave].spawnGroups.Count <= currentGroup)
        {
            EndWave();
        }
        else
        {
            timeSinceLastSpawn += Time.deltaTime;
            var group = waves[currentWave].spawnGroups[currentGroup];
            if (timeSinceLastSpawn >= group.timeUntilSpawn)
            {
                StartCoroutine(Spawn(group));

                timeSinceLastSpawn = 0;
                currentGroup++;
            }
        }
    }

    private void StartWave()
    {
        waveEnded = false;
        currentGroup = 0;
        OnWaveStarted?.Invoke();
    }

    private void EndWave()
    {
        waveEnded = true;
        timeSinceWaveEnded = 0;
        currentWave++;

        OnWaveEnded?.Invoke();
    }

    private bool CheckIfBotsRemaining()
    {
        var robots = FindObjectsOfType<Robot>();
        return robots.Any(r => !r.isDead);
    }

    private IEnumerator Spawn(RobotSpawnGroup group)
    {
        var perFrame = 4;
        var inFrame = 0;
        foreach(var robot in group.robots)
        {
            if(inFrame >= perFrame)
            {
                inFrame = 0;
                yield return null;
            }
            Instantiate(robot, GetSpawnPoint(), Quaternion.identity, transform);
            inFrame++;
        }
    }

    private Vector3 GetSpawnPoint()
    {
        return spawnPoints[UnityEngine.Random.Range(0, spawnPoints.Length)].transform.position + (Vector3)(UnityEngine.Random.insideUnitCircle * 2);
    }
}
