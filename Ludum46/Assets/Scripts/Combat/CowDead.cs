﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CowDead : MonoBehaviour
{
    public Health health;
    public int amount;
    public ThrowableItem deathDrop;

    public bool hasDone;
    public static HashSet<CowDead> AllCows = new HashSet<CowDead>();

    private void Start()
    {
        AllCows.Add(this);
    }

    private void OnDestroy()
    {
        AllCows.Remove(this);
        if(AllCows.Count == 0)
        {
            Gameover.EndGame(false);
        }
    }

    void Update()
    {
        if(hasDone)
        {
            return;
        }

        if(health.currentHealth <= 0)
        {
            DoDeath();
        }
    }

    private void DoDeath()
    {
        hasDone = true;
        for(var i = 0; i < amount; ++i)
        {
            var item = Instantiate<ThrowableItem>(deathDrop, transform.position, Quaternion.identity);
            item.waitingForPickup = true;
        }

        Economy.KillCow(GetComponentInParent<Cow>().cowName);
        Destroy(gameObject);
    }
}
