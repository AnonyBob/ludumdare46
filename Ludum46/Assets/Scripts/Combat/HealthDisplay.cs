﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthDisplay : MonoBehaviour
{
    public Image healthAmount;
    public Transform healthAmountAsTransform;
    public bool horizontal;

    public Health health;

    // Update is called once per frame
    void Update()
    {
        if(healthAmount != null)
        {
            healthAmount.fillAmount = ((float)health.currentHealth) / health.maxHealth;
        }
        else if(healthAmountAsTransform != null)
        {
            var scale = healthAmountAsTransform.localScale;
            if(horizontal)
            {
                scale.x = ((float)health.currentHealth) / health.maxHealth;
            }
            else
            {
                scale.y = ((float)health.currentHealth) / health.maxHealth;
            }

            healthAmountAsTransform.localScale = scale;
        }
    }
}
