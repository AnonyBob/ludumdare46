﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    public int maxHealth;
    public int startingHealth;

    public int currentHealth;
    public bool isDead;

    public Animator animator;

    private void Start()
    {
        currentHealth = startingHealth;
    }

    public void Deduct(int amount)
    {
        if (isDead)
        {
            return;
        }

        currentHealth -= amount;
        animator.SetTrigger("Hit");
        if(currentHealth <= 0)
        {
            currentHealth = 0;
            Die();
        }
    }

    public void Credit(int amount)
    {
        if(isDead)
        {
            return;
        }

        currentHealth += amount;
        if(currentHealth >= maxHealth)
        {
            currentHealth = maxHealth;
        }
    }

    public void Die()
    {
        isDead = true;
    }
}
