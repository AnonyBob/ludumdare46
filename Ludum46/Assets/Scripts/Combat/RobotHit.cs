﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RobotHit : MonoBehaviour
{
    public AudioSource hitSound;
    public AudioSource otherHitSound;

    public float otherHitThreshold;
    public float otherHitMax = 40f;

    public float maxVolume = 0.6f;
    public float minVolume = 0.2f;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.collider.GetComponent<Pitchfork>())
        {
            hitSound.Play();
        }
        else if(collision.relativeVelocity.magnitude > otherHitThreshold)
        {
            var magnitude = collision.relativeVelocity.magnitude;
            var percentage = Mathf.Clamp01(magnitude / (otherHitMax - otherHitThreshold));

            otherHitSound.volume = Mathf.Lerp(minVolume, maxVolume, percentage);
            otherHitSound.Play();
        }
    }
}
