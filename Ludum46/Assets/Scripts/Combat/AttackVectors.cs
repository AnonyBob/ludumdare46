﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AttackVectors : MonoBehaviour
{
    private static HashSet<AttackVectors> attackVectorCollection = new HashSet<AttackVectors>();

    public Vector3 position => transform.position;

    public AttackVector[] attackVectors = new AttackVector[1];

    public static AttackVectors GetClosest(Vector3 position)
    {
        AttackVectors closest = null;
        var closestDistance = float.MaxValue;
        foreach(var vectors in attackVectorCollection)
        {
            var distance = Vector2.Distance(position, vectors.position);
            if(distance < closestDistance)
            {
                closest = vectors;
                closestDistance = distance;
            }
        }

        return closest;
    }

    public AttackVector GetFreeVector(AttackVector previous)
    {
        var vectors = attackVectors.Where(a => a != previous || !a.claimed).ToList();
        if(vectors.Count == 0)
        {
            return null;
        }
        else
        {
            return vectors[Random.Range(0, vectors.Count)];
        }
    }

    private void Start()
    {
        attackVectorCollection.Add(this);
    }

    private void OnDestroy()
    {
        attackVectorCollection.Remove(this);
    }
}
