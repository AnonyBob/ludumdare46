﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackVector : MonoBehaviour
{
    public Vector3 position => transform.position;

    public bool claimed;
    public Robot claimer;

    public void Free(Robot claimer)
    {
        if(this.claimer == claimer)
        {
            this.claimer = null;
            this.claimed = false;
        }
    }

    public void ClaimVector(Robot claimer)
    {
        claimed = true;
        this.claimer = claimer;
    }
}
