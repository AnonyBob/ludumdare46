﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Broiler : Robot
{
    [Header("Broiler Data")]
    public int numberOfHits = 2;
    public LineRenderer laser;
    public float laserPushSpeed = 1.5f;
    public Transform laserPoint;
    public GameObject explosion;
    public float explosionWaitTime = 3f;
    public float closeEnoughDistance = 5f;

    public float attackTime = 2f;
    public float betweenDamge = 0.5f;

    public float explosionRadius = 2;
    public int explosionFoodHurt = 4;
    public int explosionHurt = 10;


    [Header("Broiler Debug")]
    public int hitsRemaining;
    public bool explosionSequenceStarted;
    public float timeSinceLastDamage = 100;
    public float timeSinceStartAttack;

    public override void Hit(Collision2D collision)
    {
        hitsRemaining--;
        if(hitsRemaining <= 0)
        {
            laser.gameObject.SetActive(false);
            body.velocity = -collision.relativeVelocity;
            body.constraints = RigidbodyConstraints2D.None;
            body.drag = body.drag * 0.5f;
            ai.enabled = false;
            Die();
        }
    }

    protected override void DoAttack()
    {
        if (target == null || attackVector == null || !CloseEnoughToVector())
        {
            laser.gameObject.SetActive(false);
            attacking = false;
            return;
        } 

        if(timeSinceLastDamage >= betweenDamge)
        {
            target.GetComponent<Health>().Deduct(damageOnAttack);
            timeSinceLastDamage = 0;
        }

        if(timeSinceStartAttack >= attackTime)
        {
            attacking = false;
            timeSinceLastDamage = 0;
            timeSinceStartAttack = 0;
            return;
        }

        body.velocity = (laserPoint.position - attackVector.position).normalized * laserPushSpeed;
        laser.gameObject.SetActive(true);

        var firstPos = laserPoint.position;
        firstPos.z = -2;
        laser.SetPosition(0, firstPos);

        var secondPos = target.position;
        secondPos.z = -2;
        laser.SetPosition(1, secondPos);

        timeSinceStartAttack += Time.deltaTime;
        timeSinceLastDamage += Time.deltaTime;
    }

    protected override void InternalUpdate()
    {
        if(!attacking)
        {
            laser.gameObject.SetActive(false);
        }
    }

    protected override bool CloseEnoughToVector()
    {
        if (attackVector != null)
        {
            return Vector2.Distance(attackVector.position, transform.position) < closeEnoughDistance;
        }

        return false;
    }

    protected override void AcquireTarget()
    {
        base.AcquireTarget();
        laser.gameObject.SetActive(false);
    }

    private void Start()
    {
        hitsRemaining = numberOfHits;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (isDead)
        {
            if(!explosionSequenceStarted)
            {
                StartCoroutine(ExplosionSequence());
            }
            return;
        }

        if(!attacking)
        {
            BounceBack(collision.contacts[0].normal);
            AcquireVector();
        }
    }

    private IEnumerator ExplosionSequence()
    {
        explosionSequenceStarted = true;
        yield return new WaitForSeconds(explosionWaitTime);

        var pos = transform.position;
        pos.z = -2;
        Instantiate(explosion, pos, Quaternion.identity, transform.parent);

        var hits = Physics2D.CircleCastAll(transform.position, explosionRadius, Vector2.zero);
        foreach(var hit in hits)
        {
            var food = hit.collider.GetComponent<FoodSource>();
            if(food != null)
            {
                food.Amount -= explosionFoodHurt;
                if(food.Amount < 0)
                {
                    food.Amount = 0;
                }

                food.RefreshVisuals();
            }

            var health = hit.collider.GetComponent<Health>();
            if(health != null)
            {
                if(health.GetComponent<Robot>() == null)
                {
                    health.Deduct(explosionHurt);
                }
            }
        }

        Destroy(gameObject);
    }
}
