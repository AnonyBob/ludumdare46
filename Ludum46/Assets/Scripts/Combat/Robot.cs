﻿using Pathfinding;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Robot : MonoBehaviour 
{
    public float timeToClean = 20f;
    public Rigidbody2D body;
    public int damageOnAttack;
    public float bounceBackSpeed;
    public float timeBetweenBounceBacks;
    public AIDestinationSetter destinationSetter;
    public AIPath ai;
    public GameObject brokenAppearance;

    [Header("Debug")]
    public bool attacking;
    public AttackVectors target;
    public AttackVector attackVector;
    public float timeSinceDead;
    public bool isDead;
    public bool bouncingBack;
    public float bounceBackTime;

    public abstract void Hit(Collision2D collision);

    protected abstract void DoAttack();

    protected virtual void InternalUpdate()
    {

    }

    private void Awake()
    {
        ai = GetComponent<AIPath>();
    }

    protected virtual void Update()
    {
        InternalUpdate();

        if(isDead)
        {
            return;
        }

        if (bouncingBack)
        {
            bounceBackTime += Time.deltaTime;
            if (bounceBackTime >= timeBetweenBounceBacks)
            {
                bouncingBack = false;
                ai.enabled = true;
            }
            return;
        }

        if (attacking && target != null)
        {
            ai.enabled = false;
            DoAttack();
        }
        else if (target != null)
        {
            ai.enabled = true;
            if (attackVector != null)
            {
                if (CloseEnoughToVector())
                {
                    attacking = true;
                    destinationSetter.target = null;
                }
                else
                {
                    MoveToAttackVector();
                }
            }
            else
            {
                AcquireVector();
            }
        }
        else
        {
            AcquireTarget();
        }
    }

    private void LateUpdate()
    {
        if(isDead)
        {
            ai.enabled = false;
            if(timeSinceDead >= timeToClean)
            {
                Destroy(gameObject);
            }

            timeSinceDead += Time.deltaTime;
        }
    }

    private void OnDestroy()
    {
        if (attackVector != null)
        {
            attackVector.Free(this);
        }
    }

    protected virtual void AcquireTarget()
    {
        target = AttackVectors.GetClosest(transform.position);
        AcquireVector();
    }

    protected virtual void AcquireVector()
    {
        if (target != null)
        {
            var nextAttackVector = target.GetFreeVector(attackVector);
            if (attackVector != null)
            {
                attackVector.Free(this);
            }

            attackVector = nextAttackVector;
            if (attackVector != null)
            {
                attackVector.ClaimVector(this);
            }
            else
            {
                target = null; //Target no longer has free vectors.
            }
        }
    }

    protected virtual void MoveToAttackVector()
    {
        if (target != null && attackVector != null)
        {
            destinationSetter.target = attackVector.transform;
        }
    }

    protected virtual bool CloseEnoughToVector()
    {
        if (attackVector != null)
        {
            return Vector2.Distance(attackVector.position, transform.position) < 0.2f;
        }

        return false;
    }

    protected void BounceBack(Vector2 direction)
    {
        ai.enabled = false;
        body.velocity = direction * bounceBackSpeed;
        bouncingBack = true;
        bounceBackTime = 0;
    }

    protected virtual void Die()
    {
        ai.enabled = false;
        isDead = true;
        body.gravityScale = 5;
        gameObject.layer = LayerMask.NameToLayer("Dead");
        brokenAppearance.SetActive(true);
    }
}
