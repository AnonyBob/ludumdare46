﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOnTimer : MonoBehaviour
{
    public float timeToDestroy = 2f;

    private void Start()
    {
        Destroy(gameObject, timeToDestroy);
    }
}
