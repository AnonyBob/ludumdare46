﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class MoneyPop : MonoBehaviour
{
    public TextMeshPro text;
    public Color increase;
    public Color decrease;
    public float timeAlive = 2f;

    private float time = 0;

    public void Setup(int amountChanged)
    {
        text.text = amountChanged.ToString("N0");
        text.color = amountChanged >= 0 ? increase : decrease;
    }

    private void Update()
    {
        time += Time.deltaTime;

        if(time >= timeAlive)
        {
            Destroy(gameObject);
        }
    }
}
