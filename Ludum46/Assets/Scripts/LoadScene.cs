﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class LoadScene : MonoBehaviour
{
    public string scene;

    public Animator transitions;

    public bool transitioning;

    public LoadSceneMode mode;

    public UnityEvent beforeLoad;
    public UnityEvent afterLoad;

    public void DoIt()
    {
        if(transitioning)
        {
            return;
        }

        if(transitions != null)
        {
            StartCoroutine(DoTransition());
        }
        else
        {
            SceneManager.LoadScene(scene);
        }
    }

    private IEnumerator DoTransition()
    {
        transitioning = true;
        transitions.SetTrigger("TransOut");
        while(!transitions.GetCurrentAnimatorStateInfo(0).IsName("Out") || transitions.IsInTransition(0))
        {
            yield return null;
        }

        beforeLoad.Invoke();
        SceneManager.LoadScene(scene, mode);
        afterLoad.Invoke();
    }
}
