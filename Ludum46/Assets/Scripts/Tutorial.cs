﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tutorial : MonoBehaviour
{
    public GameObject[] tutorials;

    public int tutorialIndex;
    public int previousTutorialIndex = -1;
    private ItemThrower thrower;
    private SelectedTool selectedTool;
    private CowHolder cowHolder;
    private SeedThrower seedThrower;
    private Truck[] trucks;

    private float timeSinceTutorial2;

    private int seedsThrown;
    private bool hasRecievedGoods;

    private static bool sawTutorial = false;

    private void Start()
    {
        thrower = FindObjectOfType<ItemThrower>();
        selectedTool = FindObjectOfType<SelectedTool>();
        cowHolder = FindObjectOfType<CowHolder>();
        seedThrower = FindObjectOfType<SeedThrower>();
        trucks = FindObjectsOfType<Truck>();

        seedThrower.OnSeedThrow += ThrowSeed;
        foreach(var truck in trucks)
        {
            truck.OnReceivedGoods += ReceiveGoods;
        }
    }

    private void ReceiveGoods()
    {
        if(tutorialIndex == 3)
        {
            hasRecievedGoods = true;
        }
    }

    private void ThrowSeed()
    {
        if(tutorialIndex == 4)
        {
            seedsThrown++;
        }
    }

    private void Update()
    {
        if(sawTutorial)
        {
            enabled = false;
        }

        if(tutorialIndex == 0)
        {
            if(Input.GetAxis("Mouse ScrollWheel") != 0)
            {
                tutorialIndex++;
            }
        }
        else if(tutorialIndex == 4)
        {
            timeSinceTutorial2 += Time.deltaTime;
            if(timeSinceTutorial2 > 5)
            {
                tutorialIndex++;
                sawTutorial = true;
            }
        }
        else if(tutorialIndex == 1)
        {
            if(thrower.currentItem == Item.Milk)
            {
                tutorialIndex++;
            }
        }
        else if(tutorialIndex == 2)
        {
            if(selectedTool.selected == Tool.Cow)
            {
                tutorialIndex ++;
            }
        }
        else if(tutorialIndex == 3)
        {
            if(hasRecievedGoods)
            {
                tutorialIndex++;
            }
        }
    }

    private void LateUpdate()
    {
        if(tutorialIndex != previousTutorialIndex)
        {
            if(previousTutorialIndex >= 0 && previousTutorialIndex < tutorials.Length)
            {
                tutorials[previousTutorialIndex].SetActive(false);
            }
            
            previousTutorialIndex = tutorialIndex;
            if (tutorials.Length > tutorialIndex)
            {
                tutorials[tutorialIndex].SetActive(true);
            }
        }
    }
}
