﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CowName
{
    Petunia,
    Margerie,
    Daisy,
    Indiana
}

public class Stats : MonoBehaviour
{
    public bool petuniaDead;
    public bool margerieDead;
    public bool daisyDead;
    public bool indianaDead;

    public int pitchforkThrown;
    public int seedsThrown;
    public int milkSold;
    public int butterSold;
    public int cheeseSold;
    public int burgersSold;

    public static Stats instance;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        Economy.OnChanged += HandleChange;
        Economy.OnCowDied += HandleCowDeath;
    }

    private void OnDestroy()
    {
        instance = null;
    }

    private void HandleCowDeath(CowName name)
    {
        switch(name)
        {
            case CowName.Petunia:
                petuniaDead = true;
                break;
            case CowName.Margerie:
                margerieDead = true;
                break;
            case CowName.Daisy:
                daisyDead = true;
                break;
            case CowName.Indiana:
                indianaDead = true;
                break;
        }
    }

    private void HandleChange(int amount, Tool tool, Item item)
    {
        switch(tool)
        {
            case Tool.Pitchfork:
                pitchforkThrown++;
                break;
            case Tool.Seed:
                seedsThrown++;
                break;
        }

        switch(item)
        {
            case Item.Butter:
                butterSold++;
                break;
            case Item.Cheese:
                cheeseSold++;
                break;
            case Item.Milk:
                milkSold++;
                break;
            case Item.Hamburger:
                burgersSold++;
                break;
        }
    }
}
